import processing.core.PApplet;
import processing.core.PVector;
import processing.data.JSONArray;
import processing.data.JSONObject;

import java.util.ArrayList;

/**
 * @author Erik Euser 2016
 */
public class Graph extends PApplet {

    private ArrayList<User> users;
    private static final int SCREEN_HEIGHT = 1000;
    private static final int SCREEN_WIDTH  = 1000;
    private static final int ELLIPSE_SIZE = 50;



    public void setup(){
        size(SCREEN_HEIGHT, SCREEN_WIDTH);
        users = loadSocialGraphData();
        noLoop();

    }

    public void draw(){
        // Title
        fill(0);
        textSize(16);
        textAlign(CENTER);
        text("Opdracht 4B - Erik Euser - 0878524", SCREEN_WIDTH/2, 50);

        // Determine position for each user node.
        for (int i = 0; i < users.size(); i++) {
            User user        = users.get(i);
            PVector position = determineNodePosition(users);
            user.setPosition(position);
        }

        // Draw relations.
        strokeWeight(1);
        fill(0);
        for (User user : users) {
            for (int i = 0; i < user.getFriends().size(); i++) {
                int friendId = user.getFriends().getInt(i);
                User friend  = users.get(friendId - 1);

                line(user.getPosition().x, user.getPosition().y, friend.getPosition().x, friend.getPosition().y);
            }
        }

        // Draw the user nodes.
        textSize(12);
        for (User user : users) {
            PVector position = user.getPosition();

            fill(255);
            ellipse(position.x, position.y, ELLIPSE_SIZE, ELLIPSE_SIZE);

            fill(0);
            text(user.getFirstName(), position.x, position.y);
        }
    }

    // Randomize de position van elke node die je drawt, check wel of die niet overlapt met een andere node.
    private PVector determineNodePosition(ArrayList<User> users){
        boolean collision = true;
        float x = 0;
        float y = 0;

        collisionLoop : while(collision){
            x = (float) Math.random() * (SCREEN_WIDTH - 100) + 50;
            y = (float) Math.random() * (SCREEN_HEIGHT - 100) + 50;

            for (User user : users) {
                if(user.getPosition() == null) continue;
                if ((x >= user.getPosition().x - (ELLIPSE_SIZE / 2) && x <= user.getPosition().x + (ELLIPSE_SIZE / 2) + 5)
                    || (y >= user.getPosition().y - (ELLIPSE_SIZE / 2) && y <= user.getPosition().y + (ELLIPSE_SIZE / 2) + 5)){
                    continue collisionLoop;
                }
            }

            collision = false;
        }

        return new PVector(x, y);
    }

    private ArrayList<User> loadSocialGraphData(){
        final JSONArray jsonArray   = loadJSONArray("../resources/social_graph.json");
        final ArrayList<User> users = new ArrayList<>();

        for (int i = 0; i < jsonArray.size(); i++) {
            final JSONObject obj = jsonArray.getJSONObject(i);

            users.add(new User(obj.getInt("id"), obj.getString("firstName"), obj.getString("surname"),
                    obj.getInt("age"), obj.getString("gender"), obj.getJSONArray("friends")));
        }

        return users;
    }
}
